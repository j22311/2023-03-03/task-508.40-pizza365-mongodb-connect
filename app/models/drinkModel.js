

//b1 khai báo thư viện mongoosedb
const { default: mongoose } = require("mongoose");

//b2: khai báo một Schema để tạo một Schema
const Schema = mongoose.Schema;

//b3 tạo đối tượng schema gồm các thuộc tính colecction trong mongbd
const drinkSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    maNuocUong: {
        type: String,
        unique: true,
        require: true,
    },
    tenNuocUong: {
        type: String,
        require: true
    },
    donGia: {
        type: Number,
        require: true
    }
})

module.exports = mongoose.model("drink",drinkSchema);