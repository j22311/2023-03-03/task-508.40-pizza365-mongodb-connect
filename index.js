//khai báo thư viện express
const express = require('express');

//khai báo thêm một đường dẫn
const path = require("path");

//khỡi tạo app express
const app = express();
const port = 8000;

//b1khai báo thư viện mongoose
var mongoose = require('mongoose');
// b2: kết nối với mongoosebd
main().catch(err => console.log(err));

async function main() {
  await mongoose.connect('mongodb://127.0.0.1:27017/CRUD_Pizza365');
  console.log('Successfully connected mongoDB');
  
}

//--bước cuối khai báo lớp model để khởi tạo các collection
const drinkModel = require('./app/models/drinkModel');  


app.get('/', (req,res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + '/views/task42.10.html'))

})
//thêm ảnh vào middleware static
app.use(express.static(__dirname + '/views'))

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})